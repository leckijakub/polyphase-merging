#include "test-unit.h"
#include <stdio.h>

#define RED "\033[0;31m"
#define NORMAL "\033[0m"
#define GREEN "\033[0;32m"

void test_fail(char* test_name)
{
    printf("[");
    printf(RED"FAIL");
    printf(NORMAL"] %s\n",test_name);
}

void test_pass(char* test_name)
{
    printf("[");
    printf(GREEN"PASS");
    printf(NORMAL"] %s\n",test_name);
}
